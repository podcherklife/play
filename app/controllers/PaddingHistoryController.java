package controllers;

import java.util.concurrent.CompletionStage;

import javax.inject.Inject;

import models.PaddingHistoryItem;
import play.mvc.Result;
import play.mvc.Controller;
import scala.xml.PrettyPrinter.Item;
import services.PadderHistoryService;
import services.PadderService;


public class PaddingHistoryController extends Controller {
	
	private PadderHistoryService padderHistoryService;

	@Inject
	public PaddingHistoryController(PadderHistoryService padderHistoryService) {
		this.padderHistoryService = padderHistoryService;
	}
	
	
    public CompletionStage<Result> getHistory(int offset, int count) {
    	return padderHistoryService.getHistoryItems(offset, count).thenApply(items -> ok(views.html.history.render(items)));
    }
    
    public CompletionStage<Result> showHistory(Long id) {
    	if (id == null) throw new IllegalArgumentException("Id should not be empty");
    	return padderHistoryService.getById(id).thenApply(mayBeItem -> {
    		return mayBeItem
    				.map(item -> ok(views.html.historyelement.render(item)))
    				.orElse(notFound());
    	});
    }
	
}