package controllers;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import javax.inject.Inject;

import com.sun.org.apache.xalan.internal.xsltc.cmdline.getopt.GetOpt;

import play.core.j.HttpExecutionContext;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Result;
import play.mvc.Controller;
import services.PadderService;


public class PadderController extends Controller {
	
	public static class PaddingForm {
		
		@play.data.validation.Constraints.Required
		private String textToPad;
		
		private String paddingResult;
		
		public PaddingForm() {
			// TODO Auto-generated constructor stub
		}
		
		public PaddingForm(String textToPad, String paddingResult) {
			super();
			this.textToPad = textToPad;
			this.paddingResult = paddingResult;
		}


		public String getTextToPad() {
			return textToPad;
		}
		
		public void setTextToPad(String textToPad) {
			this.textToPad = textToPad;
		}
		
		public String getPaddingResult() {
			return paddingResult;
		}
		
		public void setPaddingResult(String paddingResult) {
			this.paddingResult = paddingResult;
		}
	
	}
	
	private PadderService padderService;
	private FormFactory formFactory;
	
	@Inject 
	play.libs.concurrent.HttpExecutionContext ec;

	@Inject
	public PadderController(PadderService padderService, FormFactory formFactory) {
		this.padderService = padderService;
		this.formFactory = formFactory;
	}
	
	public CompletionStage<Result> renderPadder(PaddingForm inputForm) {
        return padderService.padString(inputForm.getTextToPad())
        		.thenApply(paddedString -> new PaddingForm(inputForm.getTextToPad(), paddedString))
        		.thenApplyAsync(form -> ok(views.html.index.render(formFactory.form(PaddingForm.class).fill(form))),
        				ec.current());
	}
		
	public Result emptyPaddingForm() {
        return ok(views.html.index.render(formFactory.form(PaddingForm.class)));
	}
	
	public CompletionStage<Result> doPaddingWithForm() {
		Form<PaddingForm> form = formFactory.form(PaddingForm.class).bindFromRequest();
		if (form.hasErrors()) {
			return CompletableFuture.supplyAsync(() -> badRequest(views.html.index.render(form)), ec.current());
		} else {
	        return renderPadder(form.get());
		}
	}
	
}