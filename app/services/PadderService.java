package services;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.apache.commons.lang3.StringUtils;

@Singleton
public class PadderService {
	
	private PadderHistoryService historyService;

	@Inject
	public PadderService(PadderHistoryService historyService) {
		this.historyService = historyService;
		
	}
	
	public CompletionStage<String> padString(String str) {
		return CompletableFuture.supplyAsync(() -> {
			String paddedString = StringUtils.leftPad(str, 10, "*");
			historyService.savePaddedString(str, paddedString);
			return paddedString;
		});
	}

}
