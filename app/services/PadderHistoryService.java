package services;

import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import javax.inject.Inject;
import javax.inject.Singleton;

import play.db.jpa.JPAApi;
import models.PaddingHistoryItem;

@Singleton
public class PadderHistoryService {

	private JPAApi jpa;

	@Inject
	public PadderHistoryService(JPAApi jpa) {
		this.jpa = jpa;
	}

	public void savePaddedString(String originalString, String paddedString) {
		CompletableFuture.runAsync(() -> jpa.withTransaction(() -> {
			PaddingHistoryItem historyItem = new PaddingHistoryItem();
			historyItem.setOriginalString(originalString);
			historyItem.setPaddedString(paddedString);
			jpa.em().persist(historyItem);
		}));
	}

	public CompletionStage<Collection<PaddingHistoryItem>> getHistoryItems(int offset, int count) {
		return CompletableFuture.supplyAsync(() -> jpa.withTransaction(em -> {
			return em.createQuery("from PaddingHistoryItem order by id desc", PaddingHistoryItem.class)
					.setMaxResults(count)
					.setFirstResult(offset)
					.getResultList();
		}));
	}
	
	public CompletionStage<Optional<PaddingHistoryItem>> getById(Long id) {
		return CompletableFuture.supplyAsync(() -> jpa.withTransaction(em -> {
			return Optional.ofNullable(em.find(PaddingHistoryItem.class, id));
		}));
	}

}
