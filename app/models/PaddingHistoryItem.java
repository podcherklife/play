package models;

import javax.persistence.*;

@Entity
public class PaddingHistoryItem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	public Long id;

    public String paddedString;
    
    public String originalString;
    
    public Long getId() {
		return id;
	}
    
    public void setId(Long id) {
		this.id = id;
	}
    
    public String getPaddedString() {
		return paddedString;
	}
    
    public void setPaddedString(String paddedString) {
		this.paddedString = paddedString;
	}
    
    public String getOriginalString() {
		return originalString;
	}
    
    public void setOriginalString(String originalString) {
		this.originalString = originalString;
	}
}
